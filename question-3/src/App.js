import React from 'react';
import './App.css';

const min = 0;
const max = 100;

class App extends React.Component {
  
  state = {
    value: 50
  }

  changeValue(e) {
    this.setState({
      value: e.currentTarget.value
    });
  }

  render() {
    return (
      <div className="flex flex-column">
        <div className="flex">
          <input  
            type="range" 
            name="quantity" 
            min={min} 
            max={max} 
            onInput={this.changeValue} 
            value={this.state.value}
          />
          <div className='margin-50 width-200'>
            <output for="quantity">
              {this.state.value}
            </output>
            {' '}
            <span>
              to
            </span>
          </div>
        
          <select>
            <option value="20">20</option>
            <option value="50">50</option>
            <option value="70">70</option>
          </select>
        </div>
        <div className='flex margin-50'>
          <input 
            value={this.state.value} 
            className='input-red'
          />
          <span> to </span>
          <input 
            value={this.state.value} 
            className='input-green'
          />
        </div>
        <div className='flex text'>Range is 50 to 50</div>
      </div>
    );
  }
}

export default App;
