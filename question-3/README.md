# Question 3

## Todo

1. Blue Input slider should be coupled with the red input box i.e. updating the value of slider should update the value in blue input and and vice-versa
2. Drop down should be coupled with green input box. On updating the value of dropdown, value in green input box should get updated.
3. Value in the green value should work as the max value for the slider
4. At no point of time the value of slider should be greater than the max value
5. Display text in border should reflect the values of the slider input and green input.

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

