import React from "react";
import flowers from './data/flowers';

import "./App.css";

function App() {
  const num = flowers.length;
  return (
    <div className="App">
      <h1>Welcome to smallcase</h1>
      <h2>{`Start with the ${num} flower objects`}</h2>
    </div>
  );
}

export default App;
